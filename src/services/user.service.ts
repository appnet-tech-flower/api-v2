import { UserConstant } from '../constant/users';
import bcrypt from 'bcrypt';
import UserModel from '../models/user.model';
import RandomString from 'random-string';
import { Status } from '../constant/status';
import { General } from '../constant/generals';
import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
const UserRoles = General.UserRoles;

export interface INewUser {
  email: string;
  password: string;
  type: number;
  name: string;
  phone: string;
  address: string;
  city: string;
  district: number;
  ward: number;
  registerBy: number;
  gender: number;
  role: number;
  otpCode: string;
  roleInShop: number;
  shopsId: number;
}

export const generateOTPCode = (): string => {
  return Math.floor(1000 + Math.random() * 9000).toString();
};

export const createUser = async (user: INewUser) => {
  const salt = bcrypt.genSaltSync(UserConstant.saltLength);
  const tokenEmailConfirm = RandomString({
    length: UserConstant.tokenConfirmEmailLength,
    charset: 'alphabetic'
  });

  const newUser = new UserModel({
    email: user.email,
    passwordHash: bcrypt.hashSync(user.password, salt),
    passwordSalt: salt,
    type: user.type,
    name: user.name,
    username: '',
    phone: user.phone,
    tokenEmailConfirm,
    registerBy: user.registerBy,
    status: Status.PENDING_OR_WAIT_CONFIRM,
    address: user.address || '',
    city: user.city || null,
    district: user.district || null,
    ward: user.ward || null,
    gender: user.gender || null,
    role: user.role || UserRoles.USER_ROLE_ENDUSER,
    otpCodeConfirmAccount: user.otpCode,
    googleId: null,
    facebookId: null,
    roleInShop: user.roleInShop,
    shopsId: user.shopsId
  });

  return await newUser.save();
};

export const findByEmailOrPhone = async (email: string, phone: string) => {
  return await UserModel.findOne({
    where: {
      [Op.or]: [{ email }, { phone }]
    }
  });
};

export const isValidHashPassword = (hashed: string, plainText: string) => {
  try {
    return bcrypt.compareSync(plainText, hashed);
  } catch (e) {
    return false;
  }
};

export const generateToken = (data: any): string => {
  const secretKey = General.jwtSecret;

  return jwt.sign(data, secretKey, {
    expiresIn: 60 * 60 * UserConstant.tokenExpiredInHour
  });
};
