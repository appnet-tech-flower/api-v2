import logger from './logger';
import dotenv from 'dotenv';
import { Sequelize } from 'sequelize';
import fs from 'fs';

if (fs.existsSync('.env')) {
  logger.debug('Using .env file to supply config environment variables');
  dotenv.config({ path: '.env' });
} else {
  logger.debug(
    'Using .env.example file to supply config environment variables'
  );
  dotenv.config({ path: '.env.example' }); // you can delete this after you create your own .env file!
}
export const ENVIRONMENT = process.env.NODE_ENV;
const prod = ENVIRONMENT === 'production'; // Anything else is treated as 'dev'

export const SESSION_SECRET = process.env['SESSION_SECRET'];
export const SPEED_SMS_TOKEN = process.env['SPEED_SMS_TOKEN'];
export const TWILIO_ACCOUNT_SID = process.env['TWILIO_ACCOUNT_SID'];
export const TWILIO_AUTH_TOKEN = process.env['TWILIO_AUTH_TOKEN'];
export const TWILIO_NUMBER = process.env['TWILIO_NUMBER'];

if (!SESSION_SECRET) {
  logger.error('No client secret. Set SESSION_SECRET environment variable.');
  process.exit(1);
}

export const MYSQL_CONNECTION = new Sequelize(
  process.env['MYSQL_DATABASE'],
  process.env['MYSQL_USERNAME'],
  process.env['MYSQL_PASSWORD'],
  {
    host: process.env['MYSQL_HOST'],
    dialect: 'mysql'
  }
);
