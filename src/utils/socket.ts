import { encrypt } from './encrypt';
import { SocketEvents } from '../constant/socket-event';

let io: any = null;
// const console = process['console'];

export const onDisconnect = (socket: any) => {
};

export const onConnectFn = (socket: any) => {
  console.log('User connect');

  socket.on(SocketEvents.JOIN, (data: any) => {
    if (data) {
      if (!data.userId) {
        return;
      }

      socket.join(data.userId); // using room of socket io
    } else {
      return;
    }
  });

  socket.on('disconnection', () => {
    onDisconnect(socket);
  });
};

export const init = (ioInput: any) => {
  if (ioInput) {
    io = ioInput;
    io.set('origins', '*:*');
    io.on('connection', onConnectFn);
  }
};

export const pushToUser = (userId: any, content: any) => {
  if (!io) {
    return;
  }

  io.in(userId).emit(SocketEvents.NOTIFY, JSON.stringify(content));
};

export const broadcast = (type: string, content: any) => {
  if (!io) {
    return;
  }

  type = type.toString().toUpperCase();
  io.emit(type, encrypt(JSON.stringify(content)));
};
