import { Request, Response } from 'express';
import loginValidationSchema from '../validation-schemas/user/login.schema';
import registerSchema from '../validation-schemas/user/register.schema';
import Joi from '@hapi/joi';
import { IRes } from '../interfaces/i-res';
import * as HttpStatus from 'http-status-codes';
import UserModel from '../models/user.model';
import { ResponseMessages } from '../constant/messages';
import * as UserService from '../services/user.service';
import SmsService from '../services/sms.service';
import { Status } from '../constant/status';
import { General } from '../constant/generals';
const UserTypes = General.UserTypes;
const RegisterByTypes = General.RegisterByTypes;
const UserRoles = General.UserRoles;

export const login = async (request: Request, res: Response) => {
  try {
    const { error } = loginValidationSchema.validate(request.body);
    if (error) {
      const messages = error.details.map(detail => {
        return detail.message;
      });
      const result: IRes<{}> = {
        status: HttpStatus.BAD_REQUEST,
        messages: messages,
        data: {}
      };

      return res.json(result);
    }

    const { email, password } = request.body;
    const emailOrPhone = email;
    const user = await UserService.findByEmailOrPhone(emailOrPhone, emailOrPhone);

    if (!user) {
      const result: IRes<{}> = {
        status: HttpStatus.NOT_FOUND,
        messages: [ResponseMessages.User.USER_NOT_FOUND],
        data: {}
      };

      return res.json(result);
    }

    console.log(JSON.stringify(user));

    if (!UserService.isValidHashPassword(user.passwordHash, password)) {
      const result: IRes<{}> = {
        status: HttpStatus.BAD_REQUEST,
        messages: [ResponseMessages.User.Login.WRONG_PASSWORD],
        data: {}
      };

      return res.json(result);
    }

    if (user.status !== Status.ACTIVE) {
      const result: IRes<{}> = {
        status: HttpStatus.BAD_REQUEST,
        messages: [ResponseMessages.User.Login.INACTIVE_USER],
        data: {}
      };

      return res.json(result);
    }

    const userInfoResponse = {
      _id: user.id,
      role: user.role,
      email: user.email,
      username: user.username,
      name: user.name,
      phone: user.phone,
      address: user.address,
      type: user.type,
      status: user.status,
      avatar: user.avatar,
      gender: user.gender,
      city: user.city,
      district: user.district,
      ward: user.ward,
      registerBy: user.registerBy
    }
      ;
    const token = UserService.generateToken({ _id: user.id });

    const result: IRes<{}> = {
      status: HttpStatus.OK,
      messages: [ResponseMessages.User.Login.LOGIN_SUCCESS],
      data: {
        meta: {
          token
        },
        entries: [userInfoResponse]
      }
    };

    return res.json(result);
  } catch (e) {
    console.error(e);
    const result: IRes<{}> = {
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      messages: [JSON.stringify(e)]
    };

    return res.json(result);
  }
};

export const registerNewUser = async (request: Request, res: Response) => {
  try {
    const { error } = registerSchema.validate(request.body);
    if (error) {
      const messages = error.details.map((detail: any) => {
        return detail.message;
      });

      const result: IRes<{}> = {
        status: HttpStatus.BAD_REQUEST,
        messages: messages,
        data: {}
      };

      return res.json(result);
    }
    const {
      email,
      password,
      confirmedPassword,
      name,
      username,
      phone,
      address,
      gender,
      city,
      district,
      ward
    } = request.body;

    const duplicatedPhones = await UserModel.findAll({
      where: { phone: phone }
    });
    if (duplicatedPhones.length !== 0) {
      const result: IRes<{}> = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        messages: [ResponseMessages.User.Register.PHONE_DUPLICATED],
        data: {}
      };

      return res.json(result);
    }

    if (password !== confirmedPassword) {
      const result: IRes<{}> = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        messages: [ResponseMessages.User.Register.PASSWORD_DONT_MATCH],
        data: {}
      };

      return res.json(result);
    }

    const duplicatedUsers = await UserModel.findAll({
      where: { email: email }
    });
    if (duplicatedUsers.length !== 0) {
      const result: IRes<{}> = {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        messages: [ResponseMessages.User.Register.EMAIL_DUPLICATED],
        data: {}
      };

      return res.json(result);
    }

    const otpCode = UserService.generateOTPCode();

    const newUserData: any = {
      email,
      name,
      password,
      type: UserTypes.TYPE_CUSTOMER,
      role: null,
      phone: phone,
      gender,
      city: city || null,
      district: district || null,
      ward: ward || null,
      registerBy: RegisterByTypes.NORMAL,
      address,
      otpCode,
      googleId: null,
      facebookId: null,
      roleInShop: null,
      shopsId: null
    };

    const newUser = await UserService.createUser(newUserData);

    if (
      request.user &&
      [UserRoles.USER_ROLE_MASTER, UserRoles.USER_ROLE_ADMIN].some(
        (request.user as any).role
      )
    ) {
      newUser.status = Status.ACTIVE;
      newUser.tokenEmailConfirm = '';
      await newUser.save();
    } else {
      // Send email
      // this.mailerService.sendConfirmEmail(email, name, newUser.tokenEmailConfirm);
      SmsService.sendSMS([phone], `Mã xác thực tài khoản: ${otpCode}`, '');
    }

    const result: IRes<{}> = {
      status: HttpStatus.OK,
      messages: [ResponseMessages.User.Register.REGISTER_SUCCESS],
      data: {
        meta: {},
        entries: [
          {
            email,
            name,
            username,
            phone,
            address,
            gender,
            city,
            district,
            ward
          }
        ]
      }
    };

    return res.json(result);
  } catch (e) {
    console.error(e);
    const result: IRes<{}> = {
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      messages: [JSON.stringify(e)]
    };

    return res.json(result);
  }
};
