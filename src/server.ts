import errorHandler from 'errorhandler';
import { MYSQL_CONNECTION } from './utils/secrets';
import * as dumpUser from './dump-data/user';
// import * as dumpUrlParams from "./dump-data/urlParams";
import * as socketIo from 'socket.io';
import { init } from './utils/socket';

import app from './app';

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

MYSQL_CONNECTION.authenticate()
  .then(() => {
    const server = app.listen(app.get('port'), () => {
      console.log(
        '  App is running at http://localhost:%d in %s mode',
        app.get('port'),
        app.get('env')
      );
      console.log('  Press CTRL-C to stop\n');
      dumpUser.run();
      //   dumpUrlParams.run();
      // dumpProduct.run();
    });

    // init socket
    const io = socketIo.listen(server);
    init(io);

    console.log('Connect to MySQL database successfully');
  })
  .catch((err: any) => {
    console.error('Unable to connect to MySQL database', err);
  });
